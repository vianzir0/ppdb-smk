<div class="panel panel-primary">
  <div class="panel-heading">
    <h2>Kententuan PPDB <strong class="text-success" style="color:#eee;">NAMA SEKOLAH</strong></h2>
    <span style="font-size:18px;">Unit <b>NAMA SEKOLAH</b> Tahun Ajaran <b><?php echo date('Y'); ?>-<?php echo date('Y')+1; ?></b></span>
    <!-- <hr> -->
  </div>
  <div class="panel-body">

    <ol style="color:#333;">

      <li>Setiap calon siswa wajib mengisi form pendaftaran dengan lengkap. </li>

      <li>Data-data yang diisikan pada form PPDB Online harus sesuai dengan data asli dan benar adanya.</li>

      <li>Siapkan pas foto berwarna dalam format JPG maksimal berukuran 2MB yang akan di-upload melalui form pendaftaran PPDB Online.</li>

      <li>Siapkan nilai rapor semester I dan II untuk pengisian kolom nilai yang akan dimasukkan dalam form isian nilai rapor pada PPDB Online.</li>

      <li>Calon siswa yang sudah mendaftarkan secara online akan mendapatkan Nomor Pendaftaran yang harus dicetak dan dilampirkan dalam persyaratan yang diminta oleh Panitia PPDB NAMA SEKOLAH.  </li>

      <li>Calon siswa yang sudah mendaftarkan diri melalui PPDB Online NAMA SEKOLAH akan mendapatkan Nomor Pendaftaran dan Password yang nantinya akan digunakan untuk akses informasi yang berkaitan dengan PPDB NAMA SEKOLAH.</li>

      <li>Calon siswa yang sudah mendaftarakan diri melalui PPDB Online NAMA SEKOLAH wajib menyerahkan dokumen persyaratan yang sudah ditentukan oleh Panitia PPDB NAMA SEKOLAH.</li>

      <li>Setiap calon siswa yang mendaftar wajib mengikuti tes seleksi yang diadakan oleh panitia PPDB NAMA SEKOLAH.</li>

      <li>Data yang sudah diberikan oleh Panitia PPDB NAMA SEKOLAH hanya digunakan untuk keperluan penerimaan siswa baru dan <strong class="text-danger">data tidak akan dipublikasikan serta dijaga kerahasiaannya oleh Panita PPDB</strong>.</li>

    </ol>

  </div>
</div>
